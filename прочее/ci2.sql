/*
MySQL Data Transfer
Source Host: localhost
Source Database: ci2
Target Host: localhost
Target Database: ci2
Date: 22.06.2012 10:38:24
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for battles
-- ----------------------------
DROP TABLE IF EXISTS `battles`;
CREATE TABLE `battles` (
  `name` varchar(20) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for classes
-- ----------------------------
DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `class` varchar(50) NOT NULL,
  `type` varchar(2) NOT NULL,
  `country` varchar(20) NOT NULL,
  `numGuns` tinyint(4) DEFAULT NULL,
  `bore` double DEFAULT NULL,
  `displacement` int(11) DEFAULT NULL,
  PRIMARY KEY (`class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `ID_comp` int(11) NOT NULL,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`ID_comp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='таблица для тестов';

-- ----------------------------
-- Table structure for income
-- ----------------------------
DROP TABLE IF EXISTS `income`;
CREATE TABLE `income` (
  `code` int(11) NOT NULL,
  `point` tinyint(4) NOT NULL,
  `date` datetime NOT NULL,
  `inc` decimal(12,2) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for income_o
-- ----------------------------
DROP TABLE IF EXISTS `income_o`;
CREATE TABLE `income_o` (
  `point` tinyint(4) NOT NULL,
  `date` datetime NOT NULL,
  `inc` decimal(12,2) NOT NULL,
  PRIMARY KEY (`point`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for laptop
-- ----------------------------
DROP TABLE IF EXISTS `laptop`;
CREATE TABLE `laptop` (
  `code` int(11) NOT NULL DEFAULT '0',
  `model` varchar(50) DEFAULT NULL,
  `speed_Mg` smallint(6) DEFAULT NULL,
  `ram_Mb` smallint(6) DEFAULT NULL,
  `hd_Gb` double DEFAULT NULL,
  `price` int(11) NOT NULL,
  `screen_D` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='таблица для тестов';

-- ----------------------------
-- Table structure for outcome
-- ----------------------------
DROP TABLE IF EXISTS `outcome`;
CREATE TABLE `outcome` (
  `code` int(11) NOT NULL,
  `point` tinyint(4) NOT NULL,
  `date` datetime NOT NULL,
  `out` decimal(12,2) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for outcome_o
-- ----------------------------
DROP TABLE IF EXISTS `outcome_o`;
CREATE TABLE `outcome_o` (
  `point` tinyint(4) NOT NULL,
  `date` datetime NOT NULL,
  `out` decimal(12,2) NOT NULL,
  PRIMARY KEY (`point`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for outcomes
-- ----------------------------
DROP TABLE IF EXISTS `outcomes`;
CREATE TABLE `outcomes` (
  `ship` varchar(50) NOT NULL,
  `battle` varchar(20) NOT NULL,
  `result` varchar(10) NOT NULL,
  PRIMARY KEY (`ship`,`battle`),
  KEY `FK_Outcomes_Battles` (`battle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pass_in_trip
-- ----------------------------
DROP TABLE IF EXISTS `pass_in_trip`;
CREATE TABLE `pass_in_trip` (
  `trip_no` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `ID_psg` int(11) NOT NULL,
  `place` char(10) NOT NULL,
  PRIMARY KEY (`trip_no`,`date`,`ID_psg`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='таблица для тестов';

-- ----------------------------
-- Table structure for passenger
-- ----------------------------
DROP TABLE IF EXISTS `passenger`;
CREATE TABLE `passenger` (
  `ID_psg` int(11) NOT NULL,
  `name` char(20) NOT NULL,
  PRIMARY KEY (`ID_psg`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pc
-- ----------------------------
DROP TABLE IF EXISTS `pc`;
CREATE TABLE `pc` (
  `code` int(11) NOT NULL DEFAULT '0',
  `model` varchar(50) DEFAULT NULL,
  `speed_Mg` smallint(6) DEFAULT NULL,
  `ram_Mb` smallint(6) DEFAULT NULL,
  `hd_Gb` double DEFAULT NULL,
  `cd` varchar(10) DEFAULT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for printer
-- ----------------------------
DROP TABLE IF EXISTS `printer`;
CREATE TABLE `printer` (
  `code` int(6) NOT NULL DEFAULT '0',
  `model` varchar(50) DEFAULT NULL,
  `color` char(1) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `price` double DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='таблица для тестов';

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `maker` varchar(10) DEFAULT NULL,
  `model` varchar(50) NOT NULL DEFAULT '',
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`model`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='таблица для тестов';

-- ----------------------------
-- Table structure for ships
-- ----------------------------
DROP TABLE IF EXISTS `ships`;
CREATE TABLE `ships` (
  `name` varchar(50) NOT NULL,
  `class` varchar(50) NOT NULL,
  `launched` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `FK_Ships_Classes` (`class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for trip
-- ----------------------------
DROP TABLE IF EXISTS `trip`;
CREATE TABLE `trip` (
  `trip_no` int(11) NOT NULL,
  `ID_comp` int(11) NOT NULL,
  `plane` char(10) NOT NULL,
  `town_from` char(25) NOT NULL,
  `town_to` char(25) NOT NULL,
  `time_out` datetime NOT NULL,
  `time_in` datetime NOT NULL,
  PRIMARY KEY (`trip_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='таблица для тестов';

-- ----------------------------
-- Table structure for utb
-- ----------------------------
DROP TABLE IF EXISTS `utb`;
CREATE TABLE `utb` (
  `B_DATETIME` datetime NOT NULL,
  `B_Q_ID` int(11) NOT NULL,
  `B_V_ID` int(11) NOT NULL,
  `B_VOL` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`B_DATETIME`,`B_Q_ID`,`B_V_ID`),
  KEY `FK_utB_utQ` (`B_Q_ID`),
  KEY `FK_utB_utV` (`B_V_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for utq
-- ----------------------------
DROP TABLE IF EXISTS `utq`;
CREATE TABLE `utq` (
  `Q_ID` int(11) NOT NULL,
  `Q_NAME` varchar(35) NOT NULL,
  PRIMARY KEY (`Q_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for utv
-- ----------------------------
DROP TABLE IF EXISTS `utv`;
CREATE TABLE `utv` (
  `V_ID` int(11) NOT NULL,
  `V_NAME` varchar(35) NOT NULL,
  `V_COLOR` char(1) NOT NULL,
  PRIMARY KEY (`V_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `battles` VALUES ('Guadalcanal', '1942-11-15 00:00:00');
INSERT INTO `battles` VALUES ('North Atlantic', '1941-05-25 00:00:00');
INSERT INTO `battles` VALUES ('North Cape', '1943-12-26 00:00:00');
INSERT INTO `battles` VALUES ('Surigao Strait', '1944-10-25 00:00:00');
INSERT INTO `battles` VALUES ('#Cuba62a', '1962-10-20 00:00:00');
INSERT INTO `battles` VALUES ('#Cuba62b', '1962-10-25 00:00:00');
INSERT INTO `classes` VALUES ('Bismarck', 'bb', 'Germany', '8', '15', '42000');
INSERT INTO `classes` VALUES ('Iowa', 'bb', 'USA', '9', '16', '46000');
INSERT INTO `classes` VALUES ('Kongo', 'bc', 'Japan', '8', '14', '32000');
INSERT INTO `classes` VALUES ('North Carolina', 'bb', 'USA', '12', '16', '37000');
INSERT INTO `classes` VALUES ('Renown', 'bc', 'Gt.Britain', '6', '15', '32000');
INSERT INTO `classes` VALUES ('Revenge', 'bb', 'Gt.Britain', '8', '15', '29000');
INSERT INTO `classes` VALUES ('Tennessee', 'bb', 'USA', '12', '14', '32000');
INSERT INTO `classes` VALUES ('Yamato', 'bb', 'Japan', '9', '18', '65000');
INSERT INTO `company` VALUES ('1', 'Don_avia  ');
INSERT INTO `company` VALUES ('2', 'Aeroflot  ');
INSERT INTO `company` VALUES ('3', 'Dale_avia ');
INSERT INTO `company` VALUES ('4', 'air_France');
INSERT INTO `company` VALUES ('5', 'British_AW');
INSERT INTO `income` VALUES ('1', '1', '2001-03-22 00:00:00', '15000.00');
INSERT INTO `income` VALUES ('2', '1', '2001-03-23 00:00:00', '15000.00');
INSERT INTO `income` VALUES ('3', '1', '2001-03-24 00:00:00', '3600.00');
INSERT INTO `income` VALUES ('4', '2', '2001-03-22 00:00:00', '10000.00');
INSERT INTO `income` VALUES ('5', '2', '2001-03-24 00:00:00', '1500.00');
INSERT INTO `income` VALUES ('6', '1', '2001-04-13 00:00:00', '5000.00');
INSERT INTO `income` VALUES ('7', '1', '2001-05-11 00:00:00', '4500.00');
INSERT INTO `income` VALUES ('8', '1', '2001-03-22 00:00:00', '15000.00');
INSERT INTO `income` VALUES ('9', '2', '2001-03-24 00:00:00', '1500.00');
INSERT INTO `income` VALUES ('10', '1', '2001-04-13 00:00:00', '5000.00');
INSERT INTO `income` VALUES ('11', '1', '2001-03-24 00:00:00', '3400.00');
INSERT INTO `income` VALUES ('12', '3', '2001-09-13 00:00:00', '1350.00');
INSERT INTO `income` VALUES ('13', '3', '2001-09-13 00:00:00', '1750.00');
INSERT INTO `income_o` VALUES ('1', '2001-03-22 00:00:00', '15000.00');
INSERT INTO `income_o` VALUES ('1', '2001-03-23 00:00:00', '15000.00');
INSERT INTO `income_o` VALUES ('1', '2001-03-24 00:00:00', '3400.00');
INSERT INTO `income_o` VALUES ('1', '2001-04-13 00:00:00', '5000.00');
INSERT INTO `income_o` VALUES ('1', '2001-05-11 00:00:00', '4500.00');
INSERT INTO `income_o` VALUES ('2', '2001-03-22 00:00:00', '10000.00');
INSERT INTO `income_o` VALUES ('2', '2001-03-24 00:00:00', '1500.00');
INSERT INTO `income_o` VALUES ('3', '2001-09-13 00:00:00', '11500.00');
INSERT INTO `income_o` VALUES ('3', '2001-10-02 00:00:00', '18000.00');
INSERT INTO `laptop` VALUES ('50', '3252', '120', '160', '256', '7000', '10');
INSERT INTO `laptop` VALUES ('51', '3423', '120', '80', '128', '5600', '9');
INSERT INTO `laptop` VALUES ('52', '9690', '128', '80', '128', '5000', '9');
INSERT INTO `laptop` VALUES ('53', '7824', '160', '256', '128', '6800', '12');
INSERT INTO `laptop` VALUES ('54', '6644', '133', '160', '160', '8900', '14');
INSERT INTO `laptop` VALUES ('55', '5343', '133', '160', '160', '8500', '14');
INSERT INTO `laptop` VALUES ('56', '4583', '128', '256', '160', '10500', '13');
INSERT INTO `laptop` VALUES ('57', '9690', '128', '128', '80', '4500', '13');
INSERT INTO `laptop` VALUES ('58', '6644', '256', '133', '160', '6700', '16');
INSERT INTO `laptop` VALUES ('59', '7824', '256', '133', '160', '8700', '16');
INSERT INTO `laptop` VALUES ('60', '4583', '256', '133', '160', '8700', '16');
INSERT INTO `laptop` VALUES ('61', '4583', '256', '260', '120', '7600', '12');
INSERT INTO `laptop` VALUES ('62', '3252', '128', '260', '120', '8700', '18');
INSERT INTO `laptop` VALUES ('63', '3423', '133', '160', '120', '6400', '16');
INSERT INTO `laptop` VALUES ('64', '5343', '160', '180', '120', '6350', '14');
INSERT INTO `outcome` VALUES ('1', '1', '2001-03-14 00:00:00', '15348.00');
INSERT INTO `outcome` VALUES ('2', '1', '2001-03-24 00:00:00', '3663.00');
INSERT INTO `outcome` VALUES ('3', '1', '2001-03-26 00:00:00', '1221.00');
INSERT INTO `outcome` VALUES ('4', '1', '2001-03-28 00:00:00', '2075.00');
INSERT INTO `outcome` VALUES ('5', '1', '2001-03-29 00:00:00', '2004.00');
INSERT INTO `outcome` VALUES ('6', '1', '2001-04-11 00:00:00', '3195.04');
INSERT INTO `outcome` VALUES ('7', '1', '2001-04-13 00:00:00', '4490.00');
INSERT INTO `outcome` VALUES ('8', '1', '2001-04-27 00:00:00', '3110.00');
INSERT INTO `outcome` VALUES ('9', '1', '2001-05-11 00:00:00', '2530.00');
INSERT INTO `outcome` VALUES ('10', '2', '2001-03-22 00:00:00', '1440.00');
INSERT INTO `outcome` VALUES ('11', '2', '2001-03-29 00:00:00', '7848.00');
INSERT INTO `outcome` VALUES ('12', '2', '2001-04-02 00:00:00', '2040.00');
INSERT INTO `outcome` VALUES ('13', '1', '2001-03-24 00:00:00', '3500.00');
INSERT INTO `outcome` VALUES ('14', '2', '2001-03-22 00:00:00', '1440.00');
INSERT INTO `outcome` VALUES ('15', '1', '2001-03-29 00:00:00', '2006.00');
INSERT INTO `outcome` VALUES ('16', '3', '2001-09-13 00:00:00', '1200.00');
INSERT INTO `outcome` VALUES ('17', '3', '2001-09-13 00:00:00', '1500.00');
INSERT INTO `outcome` VALUES ('18', '3', '2001-09-14 00:00:00', '1150.00');
INSERT INTO `outcome_o` VALUES ('1', '2001-03-14 00:00:00', '15348.00');
INSERT INTO `outcome_o` VALUES ('1', '2001-03-24 00:00:00', '3663.00');
INSERT INTO `outcome_o` VALUES ('1', '2001-03-26 00:00:00', '1221.00');
INSERT INTO `outcome_o` VALUES ('1', '2001-03-28 00:00:00', '2075.00');
INSERT INTO `outcome_o` VALUES ('1', '2001-03-29 00:00:00', '2004.00');
INSERT INTO `outcome_o` VALUES ('1', '2001-04-11 00:00:00', '3195.04');
INSERT INTO `outcome_o` VALUES ('1', '2001-04-13 00:00:00', '4490.00');
INSERT INTO `outcome_o` VALUES ('1', '2001-04-27 00:00:00', '3110.00');
INSERT INTO `outcome_o` VALUES ('1', '2001-05-11 00:00:00', '2530.00');
INSERT INTO `outcome_o` VALUES ('2', '2001-03-22 00:00:00', '1440.00');
INSERT INTO `outcome_o` VALUES ('2', '2001-03-29 00:00:00', '7848.00');
INSERT INTO `outcome_o` VALUES ('2', '2001-04-02 00:00:00', '2040.00');
INSERT INTO `outcome_o` VALUES ('3', '2001-09-13 00:00:00', '1500.00');
INSERT INTO `outcome_o` VALUES ('3', '2001-09-14 00:00:00', '2300.00');
INSERT INTO `outcome_o` VALUES ('3', '2002-09-16 00:00:00', '2150.00');
INSERT INTO `outcomes` VALUES ('Bismarck', 'North Atlantic', 'sunk');
INSERT INTO `outcomes` VALUES ('California', 'Surigao Strait', 'OK');
INSERT INTO `outcomes` VALUES ('Duke of York', 'North Cape', 'OK');
INSERT INTO `outcomes` VALUES ('Fuso', 'Surigao Strait', 'sunk');
INSERT INTO `outcomes` VALUES ('Hood', 'North Atlantic', 'sunk');
INSERT INTO `outcomes` VALUES ('King George V', 'North Atlantic', 'OK');
INSERT INTO `outcomes` VALUES ('Kirishima', 'Guadalcanal', 'sunk');
INSERT INTO `outcomes` VALUES ('Prince of Wales', 'North Atlantic', 'damaged');
INSERT INTO `outcomes` VALUES ('Rodney', 'North Atlantic', 'OK');
INSERT INTO `outcomes` VALUES ('Schamhorst', 'North Cape', 'sunk');
INSERT INTO `outcomes` VALUES ('South Dakota', 'Guadalcanal', 'damaged');
INSERT INTO `outcomes` VALUES ('Tennessee', 'Surigao Strait', 'OK');
INSERT INTO `outcomes` VALUES ('Washington', 'Guadalcanal', 'OK');
INSERT INTO `outcomes` VALUES ('West Virginia', 'Surigao Strait', 'OK');
INSERT INTO `outcomes` VALUES ('Yamashiro', 'Surigao Strait', 'sunk');
INSERT INTO `outcomes` VALUES ('California', 'Guadalcanal', 'damaged');
INSERT INTO `pass_in_trip` VALUES ('1100', '2003-04-29 00:00:00', '1', '1a');
INSERT INTO `pass_in_trip` VALUES ('1123', '2003-04-05 00:00:00', '3', '2a');
INSERT INTO `pass_in_trip` VALUES ('1123', '2003-04-08 00:00:00', '1', '4c');
INSERT INTO `pass_in_trip` VALUES ('1123', '2003-04-08 00:00:00', '6', '4b');
INSERT INTO `pass_in_trip` VALUES ('1124', '2003-04-02 00:00:00', '2', '2d');
INSERT INTO `pass_in_trip` VALUES ('1145', '2003-04-05 00:00:00', '3', '2c');
INSERT INTO `pass_in_trip` VALUES ('1145', '2003-04-25 00:00:00', '5', '1d');
INSERT INTO `pass_in_trip` VALUES ('1181', '2003-04-01 00:00:00', '1', '1a');
INSERT INTO `pass_in_trip` VALUES ('1181', '2003-04-01 00:00:00', '6', '1b');
INSERT INTO `pass_in_trip` VALUES ('1181', '2003-04-01 00:00:00', '8', '3c');
INSERT INTO `pass_in_trip` VALUES ('1181', '2003-04-13 00:00:00', '5', '1b');
INSERT INTO `pass_in_trip` VALUES ('1182', '2003-04-13 00:00:00', '5', '4b');
INSERT INTO `pass_in_trip` VALUES ('1182', '2003-04-13 00:00:00', '9', '6d');
INSERT INTO `pass_in_trip` VALUES ('1187', '2003-04-14 00:00:00', '8', '3a');
INSERT INTO `pass_in_trip` VALUES ('1187', '2003-04-14 00:00:00', '10', '3d');
INSERT INTO `pass_in_trip` VALUES ('1188', '2003-04-01 00:00:00', '8', '3a');
INSERT INTO `pass_in_trip` VALUES ('7771', '2005-11-04 00:00:00', '11', '4a');
INSERT INTO `pass_in_trip` VALUES ('7771', '2005-11-07 00:00:00', '11', '1b');
INSERT INTO `pass_in_trip` VALUES ('7771', '2005-11-07 00:00:00', '37', '1c');
INSERT INTO `pass_in_trip` VALUES ('7771', '2005-11-09 00:00:00', '11', '5a');
INSERT INTO `pass_in_trip` VALUES ('7771', '2005-11-14 00:00:00', '14', '4d');
INSERT INTO `pass_in_trip` VALUES ('7771', '2005-11-16 00:00:00', '14', '5d');
INSERT INTO `pass_in_trip` VALUES ('7772', '2005-11-07 00:00:00', '12', '1d');
INSERT INTO `pass_in_trip` VALUES ('7772', '2005-11-07 00:00:00', '37', '1a');
INSERT INTO `pass_in_trip` VALUES ('7772', '2005-11-29 00:00:00', '10', '3a');
INSERT INTO `pass_in_trip` VALUES ('7772', '2005-11-29 00:00:00', '13', '1b');
INSERT INTO `pass_in_trip` VALUES ('7772', '2005-11-29 00:00:00', '14', '1c');
INSERT INTO `pass_in_trip` VALUES ('7773', '2005-11-07 00:00:00', '13', '2d');
INSERT INTO `pass_in_trip` VALUES ('7778', '2005-11-05 00:00:00', '10', '2a');
INSERT INTO `pass_in_trip` VALUES ('8881', '2005-11-08 00:00:00', '37', '1d');
INSERT INTO `pass_in_trip` VALUES ('8882', '2005-11-06 00:00:00', '37', '1a');
INSERT INTO `pass_in_trip` VALUES ('8882', '2005-11-13 00:00:00', '14', '3d');
INSERT INTO `passenger` VALUES ('1', 'Bruce Willis');
INSERT INTO `passenger` VALUES ('2', 'George Clooney');
INSERT INTO `passenger` VALUES ('3', 'Kevin Costner');
INSERT INTO `passenger` VALUES ('4', 'Donald Sutherland');
INSERT INTO `passenger` VALUES ('5', 'Jennifer Lopez');
INSERT INTO `passenger` VALUES ('6', 'Ray Liotta');
INSERT INTO `passenger` VALUES ('7', 'Samuel L. Jackson');
INSERT INTO `passenger` VALUES ('8', 'Nikole Kidman');
INSERT INTO `passenger` VALUES ('9', 'Alan Rickman');
INSERT INTO `passenger` VALUES ('10', 'Kurt Russell');
INSERT INTO `passenger` VALUES ('11', 'Harrison Ford');
INSERT INTO `passenger` VALUES ('12', 'Russell Crowe');
INSERT INTO `passenger` VALUES ('13', 'Steve Martin');
INSERT INTO `passenger` VALUES ('14', 'Michael \r\n\r\nCaine');
INSERT INTO `passenger` VALUES ('15', 'Angelina Jolie');
INSERT INTO `passenger` VALUES ('16', 'Mel Gibson');
INSERT INTO `passenger` VALUES ('17', 'Michael Douglas');
INSERT INTO `passenger` VALUES ('18', 'John Travolta');
INSERT INTO `passenger` VALUES ('19', 'Sylvester Stallone');
INSERT INTO `passenger` VALUES ('20', 'Tommy Lee Jones');
INSERT INTO `passenger` VALUES ('21', 'Catherine Zeta-Jones');
INSERT INTO `passenger` VALUES ('22', 'Antonio Banderas');
INSERT INTO `passenger` VALUES ('23', 'Kim Basinger');
INSERT INTO `passenger` VALUES ('24', 'Sam Neill');
INSERT INTO `passenger` VALUES ('25', 'Gary Oldman');
INSERT INTO `passenger` VALUES ('26', 'Clint Eastwood');
INSERT INTO `passenger` VALUES ('27', 'Brad Pitt');
INSERT INTO `passenger` VALUES ('28', 'Johnny Depp');
INSERT INTO `passenger` VALUES ('29', 'Pierce Brosnan');
INSERT INTO `passenger` VALUES ('30', 'Sean Connery');
INSERT INTO `passenger` VALUES ('31', 'Bruce \r\n\r\nWillis');
INSERT INTO `passenger` VALUES ('32', 'Mullah Omar');
INSERT INTO `pc` VALUES ('30', '3244', '66', '128', '120', '4x', '24000');
INSERT INTO `pc` VALUES ('31', '3422', '100', '128', '160', '4x', '23500');
INSERT INTO `pc` VALUES ('32', '9059', '133', '128', '160', '4x', '29000');
INSERT INTO `pc` VALUES ('33', '7827', '266', '256', '120', '2x', '19000');
INSERT INTO `pc` VALUES ('34', '6654', '266', '80', '120', '2x', '15600');
INSERT INTO `pc` VALUES ('35', '5354', '266', '120', '1', '2x', '10600');
INSERT INTO `pc` VALUES ('36', '4593', '333', '64', '256', '4x', '13600');
INSERT INTO `pc` VALUES ('37', '6654', '100', '256', '256', '3x', '7500');
INSERT INTO `pc` VALUES ('38', '7827', '120', '256', '80', '3x', '8800');
INSERT INTO `pc` VALUES ('39', '3244', '130', '128', '120', '3x', '8800');
INSERT INTO `pc` VALUES ('40', '3422', '300', '160', '256', '4x', '9760');
INSERT INTO `pc` VALUES ('41', '7827', '256', '256', '256', '4x', '9000');
INSERT INTO `pc` VALUES ('42', '6654', '256', '133', '80', '2x', '7600');
INSERT INTO `pc` VALUES ('43', '4593', '160', '120', '80', '2x', '5650');
INSERT INTO `pc` VALUES ('44', '4593', '256', '120', '160', '4x', '7680');
INSERT INTO `printer` VALUES ('10', '2345', 'y', 'Laser', '6000');
INSERT INTO `printer` VALUES ('11', '2345', 'n', 'Laser', '4320');
INSERT INTO `printer` VALUES ('12', '3251', 'n', 'Jet', '5900');
INSERT INTO `printer` VALUES ('13', '3251', 'y', 'Laser', '9900');
INSERT INTO `printer` VALUES ('14', '4212', 'n', 'Matrix', '3900');
INSERT INTO `printer` VALUES ('15', '9010', 'n', 'Matrix', '1900');
INSERT INTO `printer` VALUES ('16', '7897', 'y', 'Jet', '6790');
INSERT INTO `printer` VALUES ('17', '4243', 'y', 'Jet', '5680');
INSERT INTO `printer` VALUES ('18', '4243', 'n', 'Jet', '5000');
INSERT INTO `printer` VALUES ('19', '4243', 'y', 'Laser', '7000');
INSERT INTO `printer` VALUES ('20', '4563', 'y', 'Laser', '7050');
INSERT INTO `printer` VALUES ('21', '9010', 'n', 'Matrix', '3450');
INSERT INTO `printer` VALUES ('22', '7897', 'y', 'Matrix', '6540');
INSERT INTO `printer` VALUES ('23', '3251', 'y', 'Jet', '6540');
INSERT INTO `printer` VALUES ('24', '4212', 'n', 'Jet', '4000');
INSERT INTO `printer` VALUES ('25', '4212', 'n', 'Laser', '4000');
INSERT INTO `printer` VALUES ('26', '9010', 'y', 'Laser', '5300');
INSERT INTO `printer` VALUES ('27', '7897', 'n', 'Laser', '3890');
INSERT INTO `printer` VALUES ('28', '3251', 'n', 'Matrix', '3890');
INSERT INTO `printer` VALUES ('29', '4212', 'y', 'Matrix', '3670');
INSERT INTO `product` VALUES ('Acer', '2345', 'Printer');
INSERT INTO `product` VALUES ('Acer', '3244', 'PC');
INSERT INTO `product` VALUES ('Xerox', '3251', 'Printer');
INSERT INTO `product` VALUES ('Xerox', '3252', 'Laptop');
INSERT INTO `product` VALUES ('Apple', '3422', 'PC');
INSERT INTO `product` VALUES ('Apple', '3423', 'Laptop');
INSERT INTO `product` VALUES ('Canon', '4212', 'Printer');
INSERT INTO `product` VALUES ('HP', '4243', 'Printer');
INSERT INTO `product` VALUES ('Panasonic', '4563', 'Printer');
INSERT INTO `product` VALUES ('Panasonic', '4593', 'PC');
INSERT INTO `product` VALUES ('Panasonic', '4583', 'Laptop');
INSERT INTO `product` VALUES ('Samsumg', '5343', 'Laptop');
INSERT INTO `product` VALUES ('Samsumg', '5354', 'PC');
INSERT INTO `product` VALUES ('ASUS', '6654', 'PC');
INSERT INTO `product` VALUES ('ASUS', '6644', 'Laptop');
INSERT INTO `product` VALUES ('Toshiba', '7824', 'Laptop');
INSERT INTO `product` VALUES ('Toshiba', '7827', 'PC');
INSERT INTO `product` VALUES ('Toshiba', '7897', 'Printer');
INSERT INTO `product` VALUES ('Philips', '9010', 'Printer');
INSERT INTO `product` VALUES ('Philips', '9059', 'PC');
INSERT INTO `product` VALUES ('Philips', '9690', 'Laptop');
INSERT INTO `ships` VALUES ('California', 'Tennessee', '1921');
INSERT INTO `ships` VALUES ('Haruna', 'Kongo', '1916');
INSERT INTO `ships` VALUES ('Hiei', 'Kongo', '1914');
INSERT INTO `ships` VALUES ('Iowa', 'Iowa', '1943');
INSERT INTO `ships` VALUES ('Kirishima', 'Kongo', '1915');
INSERT INTO `ships` VALUES ('Kongo', 'Kongo', '1913');
INSERT INTO `ships` VALUES ('Missouri', 'Iowa', '1944');
INSERT INTO `ships` VALUES ('Musashi', 'Yamato', '1942');
INSERT INTO `ships` VALUES ('New Jersey', 'Iowa', '1943');
INSERT INTO `ships` VALUES ('North Carolina', 'North Carolina', '1941');
INSERT INTO `ships` VALUES ('Ramillies', 'Revenge', '1917');
INSERT INTO `ships` VALUES ('Renown', 'Renown', '1916');
INSERT INTO `ships` VALUES ('Repulse', 'Renown', '1916');
INSERT INTO `ships` VALUES ('Resolution', 'Renown', '1916');
INSERT INTO `ships` VALUES ('Revenge', 'Revenge', '1916');
INSERT INTO `ships` VALUES ('Royal Oak', 'Revenge', '1916');
INSERT INTO `ships` VALUES ('Royal Sovereign', 'Revenge', '1916');
INSERT INTO `ships` VALUES ('Tennessee', 'Tennessee', '1920');
INSERT INTO `ships` VALUES ('Washington', 'North Carolina', '1941');
INSERT INTO `ships` VALUES ('Wisconsin', 'Iowa', '1944');
INSERT INTO `ships` VALUES ('Yamato', 'Yamato', '1941');
INSERT INTO `ships` VALUES ('South Dakota', 'North Carolina', '1941');
INSERT INTO `trip` VALUES ('1100', '4', 'Boeing', 'Rostov', 'Paris', '1900-01-01 14:30:00', '1900-01-01 17:50:00');
INSERT INTO `trip` VALUES ('1101', '4', 'Boeing', 'Paris', 'Rostov', '1900-01-01 08:12:00', '1900-01-01 11:45:00');
INSERT INTO `trip` VALUES ('1123', '3', 'TU-154', 'Rostov', 'Vladivostok', '1900-01-01 16:20:00', '1900-01-01 03:40:00');
INSERT INTO `trip` VALUES ('1124', '3', 'TU-154', 'Vladivostok', 'Rostov', '1900-01-01 09:00:00', '1900-01-01 19:50:00');
INSERT INTO `trip` VALUES ('1145', '2', 'IL-86', 'Moscow', 'Rostov', '1900-01-01 09:35:00', '1900-01-01 11:23:00');
INSERT INTO `trip` VALUES ('1146', '2', 'IL-86', 'Rostov', 'Moscow', '1900-01-01 17:55:00', '1900-01-01 20:01:00');
INSERT INTO `trip` VALUES ('1181', '1', 'TU-134', 'Rostov', 'Moscow', '1900-01-01 06:12:00', '0000-00-00 00:00:00');
INSERT INTO `trip` VALUES ('1182', '1', 'TU-134', 'Moscow', 'Rostov', '1900-01-01 12:35:00', '1900-01-01 14:30:00');
INSERT INTO `trip` VALUES ('1187', '1', 'TU-134', 'Rostov', 'Moscow', '1900-01-01 15:42:00', '1900-01-01 17:39:00');
INSERT INTO `trip` VALUES ('1188', '1', 'TU-134', 'Moscow', 'Rostov', '1900-01-01 22:50:00', '1900-01-01 00:48:00');
INSERT INTO `trip` VALUES ('1195', '1', 'TU-154', 'Rostov', 'Moscow', '1900-01-01 23:30:00', '1900-01-01 01:11:00');
INSERT INTO `trip` VALUES ('1196', '1', 'TU-154', 'Moscow', 'Rostov', '1900-01-01 04:00:00', '1900-01-01 05:45:00');
INSERT INTO `trip` VALUES ('7771', '5', 'Boeing', 'London', 'Singapore', '1900-01-01 01:00:00', '1900-01-01 11:00:00');
INSERT INTO `trip` VALUES ('7772', '5', 'Boeing', 'Singapore', 'London', '1900-01-01 12:00:00', '1900-01-01 02:00:00');
INSERT INTO `trip` VALUES ('7773', '5', 'Boeing', 'London', 'Singapore', '1900-01-01 03:00:00', '1900-01-01 13:00:00');
INSERT INTO `trip` VALUES ('7774', '5', 'Boeing', 'Singapore', 'London', '1900-01-01 14:00:00', '1900-01-01 06:00:00');
INSERT INTO `trip` VALUES ('7775', '5', 'Boeing', 'London', 'Singapore', '1900-01-01 09:00:00', '1900-01-01 20:00:00');
INSERT INTO `trip` VALUES ('7776', '5', 'Boeing', 'Singapore', 'London', '1900-01-01 18:00:00', '1900-01-01 08:00:00');
INSERT INTO `trip` VALUES ('7777', '5', 'Boeing', 'London', 'Singapore', '1900-01-01 18:00:00', '1900-01-01 06:00:00');
INSERT INTO `trip` VALUES ('7778', '5', 'Boeing', 'Singapore', 'London', '1900-01-01 22:00:00', '1900-01-01 12:00:00');
INSERT INTO `trip` VALUES ('8881', '5', 'Boeing', 'London', 'Paris', '1900-01-01 03:00:00', '1900-01-01 04:00:00');
INSERT INTO `trip` VALUES ('8882', '5', 'Boeing', 'Paris', 'London', '1900-01-01 22:00:00', '1900-01-01 23:00:00');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:01', '1', '1', '155');
INSERT INTO `utb` VALUES ('2003-06-23 01:12:02', '1', '1', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:03', '2', '2', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:04', '3', '3', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:05', '1', '4', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:06', '2', '5', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:07', '3', '6', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:08', '1', '7', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:09', '2', '8', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:10', '3', '9', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:11', '4', '10', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:12', '5', '11', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:13', '5', '12', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:14', '5', '13', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:15', '5', '14', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:16', '5', '15', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:17', '5', '16', '205');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:18', '6', '10', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:19', '6', '17', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:20', '6', '18', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:21', '6', '19', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:22', '7', '17', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:23', '7', '20', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:24', '7', '21', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:25', '7', '22', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:26', '8', '10', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:27', '9', '23', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:28', '9', '24', '255');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:29', '9', '25', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:30', '9', '26', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:31', '10', '25', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:31', '10', '26', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:33', '10', '27', '10');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:34', '10', '28', '10');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:35', '10', '29', '245');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:36', '10', '30', '245');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:37', '11', '31', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:38', '11', '32', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:39', '11', '33', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:40', '11', '34', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:41', '11', '35', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:42', '11', '36', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:43', '12', '31', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:44', '12', '32', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:45', '12', '33', '155');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:46', '12', '34', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:47', '12', '35', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:12:48', '12', '36', '100');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:01', '4', '37', '20');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:02', '8', '38', '20');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:03', '13', '39', '123');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:04', '14', '39', '111');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:05', '14', '40', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:06', '15', '41', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:07', '15', '41', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:08', '15', '42', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:09', '15', '42', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:10', '16', '42', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:11', '16', '42', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:12', '16', '43', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:13', '16', '43', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:14', '16', '47', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:15', '17', '44', '10');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:16', '17', '44', '10');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:17', '17', '45', '10');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:18', '17', '45', '10');
INSERT INTO `utb` VALUES ('2003-02-01 01:13:19', '18', '45', '10');
INSERT INTO `utb` VALUES ('2003-03-01 01:13:20', '18', '45', '10');
INSERT INTO `utb` VALUES ('2003-04-01 01:13:21', '18', '46', '10');
INSERT INTO `utb` VALUES ('2003-05-01 01:13:22', '18', '46', '10');
INSERT INTO `utb` VALUES ('2003-06-11 01:13:23', '19', '44', '10');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:24', '19', '44', '10');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:25', '19', '45', '10');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:26', '19', '45', '10');
INSERT INTO `utb` VALUES ('2003-02-01 01:13:27', '20', '45', '10');
INSERT INTO `utb` VALUES ('2003-03-01 01:13:28', '20', '45', '10');
INSERT INTO `utb` VALUES ('2003-04-01 01:13:29', '20', '46', '10');
INSERT INTO `utb` VALUES ('2003-05-01 01:13:30', '20', '46', '10');
INSERT INTO `utb` VALUES ('2003-02-01 01:13:31', '21', '49', '50');
INSERT INTO `utb` VALUES ('2003-02-02 01:13:32', '21', '49', '50');
INSERT INTO `utb` VALUES ('2003-02-03 01:13:33', '21', '50', '50');
INSERT INTO `utb` VALUES ('2003-02-04 01:13:34', '21', '50', '50');
INSERT INTO `utb` VALUES ('2003-02-05 01:13:35', '21', '48', '1');
INSERT INTO `utb` VALUES ('2000-01-01 01:13:36', '22', '50', '50');
INSERT INTO `utb` VALUES ('2001-01-01 01:13:37', '22', '50', '50');
INSERT INTO `utb` VALUES ('2002-01-01 01:13:38', '22', '51', '50');
INSERT INTO `utb` VALUES ('2002-06-01 01:13:39', '22', '51', '50');
INSERT INTO `utb` VALUES ('2003-01-01 01:13:05', '4', '37', '185');
INSERT INTO `utq` VALUES ('1', 'Square # 01');
INSERT INTO `utq` VALUES ('2', 'Square # 02');
INSERT INTO `utq` VALUES ('3', 'Square # 03');
INSERT INTO `utq` VALUES ('4', 'Square # 04');
INSERT INTO `utq` VALUES ('5', 'Square # 05');
INSERT INTO `utq` VALUES ('6', 'Square # 06');
INSERT INTO `utq` VALUES ('7', 'Square # 07');
INSERT INTO `utq` VALUES ('8', 'Square # 08');
INSERT INTO `utq` VALUES ('9', 'Square # 09');
INSERT INTO `utq` VALUES ('10', 'Square # 10');
INSERT INTO `utq` VALUES ('11', 'Square # 11');
INSERT INTO `utq` VALUES ('12', 'Square # 12');
INSERT INTO `utq` VALUES ('13', 'Square # 13');
INSERT INTO `utq` VALUES ('14', 'Square # 14');
INSERT INTO `utq` VALUES ('15', 'Square # 15');
INSERT INTO `utq` VALUES ('16', 'Square # 16');
INSERT INTO `utq` VALUES ('17', 'Square # 17');
INSERT INTO `utq` VALUES ('18', 'Square # 18');
INSERT INTO `utq` VALUES ('19', 'Square # 19');
INSERT INTO `utq` VALUES ('20', 'Square # 20');
INSERT INTO `utq` VALUES ('21', 'Square # 21');
INSERT INTO `utq` VALUES ('22', 'Square # 22');
INSERT INTO `utq` VALUES ('23', 'Square # 23');
INSERT INTO `utq` VALUES ('25', 'Square # 25');
INSERT INTO `utv` VALUES ('1', 'Balloon # 01', 'R');
INSERT INTO `utv` VALUES ('2', 'Balloon # 02', 'R');
INSERT INTO `utv` VALUES ('3', 'Balloon # 03', 'R');
INSERT INTO `utv` VALUES ('4', 'Balloon # 04', 'G');
INSERT INTO `utv` VALUES ('5', 'Balloon # 05', 'G');
INSERT INTO `utv` VALUES ('6', 'Balloon # 06', 'G');
INSERT INTO `utv` VALUES ('7', 'Balloon # 07', 'B');
INSERT INTO `utv` VALUES ('8', 'Balloon # 08', 'B');
INSERT INTO `utv` VALUES ('9', 'Balloon # 09', 'B');
INSERT INTO `utv` VALUES ('10', 'Balloon # 10', 'R');
INSERT INTO `utv` VALUES ('11', 'Balloon # 11', 'R');
INSERT INTO `utv` VALUES ('12', 'Balloon # 12', 'R');
INSERT INTO `utv` VALUES ('13', 'Balloon # 13', 'G');
INSERT INTO `utv` VALUES ('14', 'Balloon # 14', 'G');
INSERT INTO `utv` VALUES ('15', 'Balloon # 15', 'B');
INSERT INTO `utv` VALUES ('16', 'Balloon # 16', 'B');
INSERT INTO `utv` VALUES ('17', 'Balloon # 17', 'R');
INSERT INTO `utv` VALUES ('18', 'Balloon # 18', 'G');
INSERT INTO `utv` VALUES ('19', 'Balloon # 19', 'B');
INSERT INTO `utv` VALUES ('20', 'Balloon # 20', 'R');
INSERT INTO `utv` VALUES ('21', 'Balloon # 21', 'G');
INSERT INTO `utv` VALUES ('22', 'Balloon # 22', 'B');
INSERT INTO `utv` VALUES ('23', 'Balloon # 23', 'R');
INSERT INTO `utv` VALUES ('24', 'Balloon # 24', 'G');
INSERT INTO `utv` VALUES ('25', 'Balloon # 25', 'B');
INSERT INTO `utv` VALUES ('26', 'Balloon # 26', 'B');
INSERT INTO `utv` VALUES ('27', 'Balloon # 27', 'R');
INSERT INTO `utv` VALUES ('28', 'Balloon # 28', 'G');
INSERT INTO `utv` VALUES ('29', 'Balloon # 29', 'R');
INSERT INTO `utv` VALUES ('30', 'Balloon # 30', 'G');
INSERT INTO `utv` VALUES ('31', 'Balloon # 31', 'R');
INSERT INTO `utv` VALUES ('32', 'Balloon # 32', 'G');
INSERT INTO `utv` VALUES ('33', 'Balloon # 33', 'B');
INSERT INTO `utv` VALUES ('34', 'Balloon # 34', 'R');
INSERT INTO `utv` VALUES ('35', 'Balloon # 35', 'G');
INSERT INTO `utv` VALUES ('36', 'Balloon # 36', 'B');
INSERT INTO `utv` VALUES ('37', 'Balloon # 37', 'R');
INSERT INTO `utv` VALUES ('38', 'Balloon # 38', 'G');
INSERT INTO `utv` VALUES ('39', 'Balloon # 39', 'B');
INSERT INTO `utv` VALUES ('40', 'Balloon # 40', 'R');
INSERT INTO `utv` VALUES ('41', 'Balloon # 41', 'R');
INSERT INTO `utv` VALUES ('42', 'Balloon # 42', 'G');
INSERT INTO `utv` VALUES ('43', 'Balloon # 43', 'B');
INSERT INTO `utv` VALUES ('44', 'Balloon # 44', 'R');
INSERT INTO `utv` VALUES ('45', 'Balloon # 45', 'G');
INSERT INTO `utv` VALUES ('46', 'Balloon # 46', 'B');
INSERT INTO `utv` VALUES ('47', 'Balloon # 47', 'B');
INSERT INTO `utv` VALUES ('48', 'Balloon # 48', 'G');
INSERT INTO `utv` VALUES ('49', 'Balloon # 49', 'R');
INSERT INTO `utv` VALUES ('50', 'Balloon # 50', 'G');
INSERT INTO `utv` VALUES ('51', 'Balloon # 51', 'B');
INSERT INTO `utv` VALUES ('52', 'Balloon # 52', 'R');
INSERT INTO `utv` VALUES ('53', 'Balloon # 53', 'G');
INSERT INTO `utv` VALUES ('54', 'Balloon # 54', 'B');
