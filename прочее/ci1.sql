/*
MySQL Data Transfer
Source Host: localhost
Source Database: ci1
Target Host: localhost
Target Database: ci1
Date: 23.06.2012 0:20:33
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `user_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(65) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `access_level` tinyint(1) DEFAULT '0',
  `first_name` varchar(65) NOT NULL,
  `last_name` varchar(65) NOT NULL,
  `ot_name` varchar(65) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='инфо о пользователе';

-- ----------------------------
-- Table structure for olymp
-- ----------------------------
DROP TABLE IF EXISTS `olymp`;
CREATE TABLE `olymp` (
  `olymp_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `olymp_name` varchar(65) DEFAULT NULL,
  `date_olymp` date DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`olymp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for opros
-- ----------------------------
DROP TABLE IF EXISTS `opros`;
CREATE TABLE `opros` (
  `variant_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `variant` text,
  PRIMARY KEY (`variant_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for podchet
-- ----------------------------
DROP TABLE IF EXISTS `podchet`;
CREATE TABLE `podchet` (
  `user_id` int(6) unsigned NOT NULL DEFAULT '0',
  `zapros_id` int(6) unsigned NOT NULL DEFAULT '0',
  `olymp_bal` int(3) unsigned DEFAULT NULL,
  `date_podchet` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `olymp_id` int(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`zapros_id`,`olymp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for podchet_ball
-- ----------------------------
DROP TABLE IF EXISTS `podchet_ball`;
CREATE TABLE `podchet_ball` (
  `user_id` int(6) unsigned NOT NULL DEFAULT '0',
  `zapros_id` int(6) unsigned NOT NULL DEFAULT '0',
  `test_ball` int(3) unsigned DEFAULT NULL,
  `date_podchet_ball` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`zapros_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for podchet_opros
-- ----------------------------
DROP TABLE IF EXISTS `podchet_opros`;
CREATE TABLE `podchet_opros` (
  `user_id` int(6) unsigned NOT NULL DEFAULT '0',
  `variant_id` int(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for podchet_test
-- ----------------------------
DROP TABLE IF EXISTS `podchet_test`;
CREATE TABLE `podchet_test` (
  `user_id` int(6) unsigned NOT NULL DEFAULT '0',
  `quest_id` int(6) unsigned NOT NULL DEFAULT '0',
  `teor_ball` int(6) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`quest_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `test_status` tinyint(1) unsigned DEFAULT NULL,
  `opros_status` tinyint(1) DEFAULT NULL,
  `opros_name` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for test_answer
-- ----------------------------
DROP TABLE IF EXISTS `test_answer`;
CREATE TABLE `test_answer` (
  `answer_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `quest_id` int(6) unsigned NOT NULL DEFAULT '0',
  `answer` text,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`,`quest_id`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for test_quest
-- ----------------------------
DROP TABLE IF EXISTS `test_quest`;
CREATE TABLE `test_quest` (
  `quest_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `quest` text,
  PRIMARY KEY (`quest_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for zapros
-- ----------------------------
DROP TABLE IF EXISTS `zapros`;
CREATE TABLE `zapros` (
  `zapros_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `zadanie` varchar(255) DEFAULT NULL,
  `col` tinyint(3) unsigned DEFAULT NULL,
  `row` tinyint(3) unsigned DEFAULT NULL,
  `otvet_zaprosa_1k1` text,
  `zapros_1k1` varchar(255) DEFAULT NULL,
  `zapros_ball` tinyint(1) unsigned DEFAULT NULL,
  `zapros_lvl` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `status` tinyint(1) unsigned DEFAULT '2',
  `consumm` text,
  PRIMARY KEY (`zapros_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='col+row+otvet\r\nstat=0/1/2\r\n1к1\r\nball=1/2/3';

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `account` VALUES ('2', 'qwerty', 'd8578edf8458ce06fbc5bb76a58c5ca4', '0', 'Петр', 'Вейнберг', 'Исаевич', '2012-06-10 17:36:32', null);
INSERT INTO `account` VALUES ('1', 'kis', 'f7dae0277d6a5ffb01a0a05a6ff964fe', '1', 'имя', 'фамилия', 'отчество', '2012-06-10 17:20:27', 'kis795265@yandex.ru');
INSERT INTO `account` VALUES ('3', 'legion', '960db2ed82202a9706b97775a4269378', '0', 'Александр', 'Пушкин', 'Сергеевич', '2012-06-10 17:26:20', 're@gi.on');
INSERT INTO `account` VALUES ('4', 'qazwsx', '76419c58730d9f35de7ac538c2fd6737', '0', 'Константин', 'Батюшков', 'Николаевич', '2012-06-10 17:35:53', 'qaz@wsx.ru');
INSERT INTO `account` VALUES ('5', 'admin_1', 'e10adc3949ba59abbe56e057f20f883e', '0', 'Михаил', 'Лермонтов', 'Юрьевич', '2012-06-10 17:57:44', 'admin@kis.com');
INSERT INTO `account` VALUES ('6', 'fetaksa', '558a9ca672ab9a87e0d23293ad829ab6', '0', 'Афанасий', 'Фет', 'Афанасьевич', '2012-06-10 17:56:37', 'fet@re.ru');
INSERT INTO `account` VALUES ('7', 'student', 'cd73502828457d15655bbd7a63fb0bc8', '0', 'Михаил', 'Троянов', 'Федотович', '2012-06-10 18:01:12', 'st@u.dent');
INSERT INTO `account` VALUES ('8', 'admin_kis', '4aac8e0c1886c1bab23146dc5bb56e76', '1', 'Василий', 'Пупкин', 'Дмитриевич', '2012-06-10 18:20:48', 'admin@kis.com');
INSERT INTO `account` VALUES ('9', 'qazxcv', '1bba93f43f200abb809cafcd503d9147', '0', 'чсм', 'йфя', 'цыув', '2012-06-21 19:36:52', 'qaz@xcv.ru');
INSERT INTO `olymp` VALUES ('1', 'первая_олимпиада', '2012-03-14', 'самая первая олимпиада', '0');
INSERT INTO `opros` VALUES ('1', 'all_good');
INSERT INTO `opros` VALUES ('2', 'хорошо');
INSERT INTO `opros` VALUES ('3', 'ололо');
INSERT INTO `opros` VALUES ('4', 'ну так себе');
INSERT INTO `podchet` VALUES ('3', '24', '300', '2012-05-31 02:06:57', '1');
INSERT INTO `podchet` VALUES ('1', '10', '200', '2012-06-10 18:02:30', '1');
INSERT INTO `podchet` VALUES ('2', '22', '300', '0000-00-00 00:00:00', '1');
INSERT INTO `podchet` VALUES ('4', '2', '95', '2012-06-10 18:03:12', '1');
INSERT INTO `podchet` VALUES ('4', '5', '100', '0000-00-00 00:00:00', '1');
INSERT INTO `podchet` VALUES ('5', '16', '200', '0000-00-00 00:00:00', '1');
INSERT INTO `podchet_ball` VALUES ('1', '19', '300', '2012-06-21 22:51:18');
INSERT INTO `podchet_ball` VALUES ('1', '18', '300', '2012-06-21 22:51:33');
INSERT INTO `podchet_ball` VALUES ('1', '22', '300', '2012-06-21 22:59:04');
INSERT INTO `podchet_ball` VALUES ('1', '20', '300', '2012-06-21 22:54:10');
INSERT INTO `podchet_ball` VALUES ('1', '25', '300', '2012-06-21 22:54:50');
INSERT INTO `podchet_ball` VALUES ('1', '21', '300', '2012-06-21 22:58:45');
INSERT INTO `podchet_opros` VALUES ('3', '4');
INSERT INTO `podchet_opros` VALUES ('1', '3');
INSERT INTO `status` VALUES ('1', '1', 'как вам\r\nоформление\r\nсайта?');
INSERT INTO `test_answer` VALUES ('1', '1', 'выбор всего', '1');
INSERT INTO `test_answer` VALUES ('2', '1', 'выбор одного', '0');
INSERT INTO `test_answer` VALUES ('3', '2', 'выборка записей', '1');
INSERT INTO `test_answer` VALUES ('4', '2', 'удаление записей', '0');
INSERT INTO `test_quest` VALUES ('1', 'выбирете значение знака звездочка');
INSERT INTO `test_quest` VALUES ('2', 'как называеться оператор выборки');
INSERT INTO `zapros` VALUES ('1', 'Найдите номер модели, скорость, и размер жесткого диска для всех ПК стоимостью менее 10000 дол.\r\nВывести: model,speed,hd', '4', '8', '6654,100,256,75007827,120,80,88003244,130,120,88003422,300,256,97607827,256,256,90006654,256,80,76004593,160,80,56504593,256,160,7680', 'SELECT\r\npc.model,\r\npc.speed_Mg,\r\npc.hd_Gb,\r\npc.price\r\nfrom pc\r\nWHERE\r\npc.price <  \'10000\'', '1', '1', '1', '7.38546801429E+39');
INSERT INTO `zapros` VALUES ('2', 'Найдите производителей принтеров. Вывести : maker', '1', '7', 'AcerXeroxCanonPhilipsToshibaHPPanasonic', 'SELECT distinct maker from printer,product where printer.model=product.model\r\n', '1', '1', '1', '1.19807101598E+39');
INSERT INTO `zapros` VALUES ('3', 'Найдите номер модели, объем памяти и размеры экранов ПК-блокнотов, цена которых превышает 1000 дол.\r\n', '3', '15', '3252,160,103423,80,99690,80,97824,256,126644,160,145343,160,144583,256,139690,128,136644,133,167824,133,164583,133,164583,260,123252,260,183423,160,165343,180,14', 'SELECT\r\nlaptop.model,\r\nlaptop.ram_Mb,\r\nlaptop.screen_D\r\nFROM laptop\r\nwhere price>1000', '1', '1', '1', '8.31578401824E+39');
INSERT INTO `zapros` VALUES ('4', 'Найдите все записи таблицы Printer для цветных принтеров.\r\n', '5', '10', '10,2345,y,Laser,600013,3251,y,Laser,990016,7897,y,Jet,679017,4243,y,Jet,568019,4243,y,Laser,700020,4563,y,Laser,705022,7897,y,Matrix,654023,3251,y,Jet,654026,9010,y,Laser,530029,4212,y,Matrix,3670', 'SELECT * FROM printer where color=\'y\'', '1', '1', '1', '8.03001163734E+39');
INSERT INTO `zapros` VALUES ('5', 'Найдите номер модели, скорость и размер жесткого диска ПК, имеющих 2x или 3x CD и цену менее 8000 дол.\r\n', '3', '3', '6654,100,2566654,256,804593,160,80', 'SELECT\r\npc.model,\r\npc.speed_Mg,\r\npc.hd_Gb\r\nfrom pc\r\nwhere cd in(\'2x\',\'3x\') and price<8000\r\n', '1', '1', '1', '2.21041695766E+39');
INSERT INTO `zapros` VALUES ('6', 'Укажите производителя и скорость для тех ПК-блокнотов, которые имеют жесткий диск объемом не менее 10 Гбайт.\r\n', '2', '13', 'Xerox,120Apple,120Philips,128Toshiba,160ASUS,133Samsumg,133Panasonic,128ASUS,256Toshiba,256Panasonic,256Xerox,128Apple,133Samsumg,160', 'select distinct maker,speed_mg from laptop, product where hd_gb>10 and laptop.model=product.model', '1', '1', '1', '5.44010944046E+39');
INSERT INTO `zapros` VALUES ('7', 'Найдите производителей ПК с процессором не менее 233 Мгц. Вывести: Maker\r\n', '1', '5', 'ApplePanasonicSamsumgASUSToshiba', 'select distinct maker from product where model in (select model from pc where speed_mg >233)\r\n', '1', '1', '1', '6.97903272297E+38');
INSERT INTO `zapros` VALUES ('8', 'Найдите принтеры, имеющие самую высокую цену. Вывести: model, price\r\n', '2', '1', '3251,9900', 'select  model, price from  printer where price=(select max(price) from printer)', '1', '1', '1', '2.91461984765E+38');
INSERT INTO `zapros` VALUES ('9', 'Найти тех производителей ПК, все модели ПК которых имеются в таблице PC.\r\n', '1', '7', 'AcerApplePhilipsToshibaASUSSamsumgPanasonic', 'select distinct maker from product, pc where product.model=pc.model\r\n', '1', '1', '1', '9.97831342174E+38');
INSERT INTO `zapros` VALUES ('10', 'Найдите производителя, выпускающего ПК, но не ПК-блокноты.\r\n', '1', '3', 'AcerCanonHP', 'SELECT distinct maker FROM product p where type!=\'PC\' and maker not in (select maker from product where type=\'Laptop\') \r\n', '2', '2', '1', '5.0663913333E+38');
INSERT INTO `zapros` VALUES ('18', 'Найдите производителей, которые производили бы как ПК со скоростью не менее 300 МГц, так и ПК-блокноты со скоростью не менее 300 МГц. Вывести: Maker\r\n', '1', '1', 'Panasonic', 'select maker from product where model in (select model from pc where speed_mg>300) or model in (select model from laptop where speed_mg >300)\r\n', '3', '3', '1', '1.03279558533E+38');
INSERT INTO `zapros` VALUES ('11', 'Найдите пары моделей PC, имеющих одинаковую ram_mb.  Порядок вывода: модель с большим номером, модель с меньшим номером, скорость и ram_mb.', '4', '14', '3422,6654,100,1284593,6654,256,1204593,7827,256,1205354,6654,266,1205354,7827,266,1206654,3422,100,2566654,7827,256,1336654,7827,266,806654,4593,256,1336654,5354,266,807827,4593,256,2567827,5354,266,2567827,6654,256,2567827,6654,266,256', 'select p1.model,p2. model, p1.speed_mg, p1.ram_mb from pc p1, pc p2 where p1.speed_mg=p2.speed_mg and p1.model!=p2.model order by p1.model\r\n', '2', '2', '1', '1.20313728847E+40');
INSERT INTO `zapros` VALUES ('12', 'Найдите ПК-блокноты, скорость которых меньше скорости любого из ПК. Вывести: type, model, speed_mg\r\n', '3', '129', 'laptop,3252,120laptop,3423,120laptop,9690,128laptop,4583,128laptop,9690,128laptop,3252,128laptop,3252,120laptop,3423,120laptop,9690,128laptop,7824,160laptop,6644,133laptop,5343,133laptop,4583,128laptop,9690,128laptop,6644,256laptop,7824,256laptop,4583,256laptop,4583,256laptop,3252,128laptop,3423,133laptop,5343,160laptop,3252,120laptop,3423,120laptop,9690,128laptop,7824,160laptop,6644,133laptop,5343,133laptop,4583,128laptop,9690,128laptop,6644,256laptop,7824,256laptop,4583,256laptop,4583,256laptop,3252,128laptop,3423,133laptop,5343,160laptop,3252,120laptop,3423,120laptop,9690,128laptop,7824,160laptop,6644,133laptop,5343,133laptop,4583,128laptop,9690,128laptop,6644,256laptop,7824,256laptop,4583,256laptop,4583,256laptop,3252,128laptop,3423,133laptop,5343,160laptop,3252,120laptop,3423,120laptop,9690,128laptop,7824,160laptop,6644,133laptop,5343,133laptop,4583,128laptop,9690,128laptop,6644,256laptop,7824,256laptop,4583,256laptop,4583,256laptop,3252,128laptop,3423,133laptop,5343,160laptop,3252,120laptop,3423,120laptop,9690,128laptop,4583,128laptop,9690,128laptop,3252,128laptop,3252,120laptop,3423,120laptop,9690,128laptop,7824,160laptop,6644,133laptop,5343,133laptop,4583,128laptop,9690,128laptop,6644,256laptop,7824,256laptop,4583,256laptop,4583,256laptop,3252,128laptop,3423,133laptop,5343,160laptop,3252,120laptop,3423,120laptop,9690,128laptop,7824,160laptop,6644,133laptop,5343,133laptop,4583,128laptop,9690,128laptop,3252,128laptop,3423,133laptop,5343,160laptop,3252,120laptop,3423,120laptop,9690,128laptop,7824,160laptop,6644,133laptop,5343,133laptop,4583,128laptop,9690,128laptop,3252,128laptop,3423,133laptop,5343,160laptop,3252,120laptop,3423,120laptop,9690,128laptop,6644,133laptop,5343,133laptop,4583,128laptop,9690,128laptop,3252,128laptop,3423,133laptop,3252,120laptop,3423,120laptop,9690,128laptop,7824,160laptop,6644,133laptop,5343,133laptop,4583,128laptop,9690,128laptop,3252,128laptop,3423,133laptop,5343,160', 'select  \'laptop\' as type, laptop.model, laptop.speed_mg from  laptop, pc where laptop.speed_mg< pc.speed_Mg', '2', '2', '1', '4.90146341134E+40');
INSERT INTO `zapros` VALUES ('13', 'Найдите производителей самых дешевых цветных принтеров. Вывести: maker, price\r\n', '2', '1', 'Canon,3670', 'select maker, price from product, printer where product.model=printer.model and  price=(select min(price) from printer where color=\'y\')\r\n', '2', '2', '1', '3.16892762612E+38');
INSERT INTO `zapros` VALUES ('14', 'Для каждого производителя найдите средний размер экрана выпускаемых им ПК-блокнотов. Вывести: maker, средний размер экрана.\r\n', '2', '7', 'Apple,12.5000ASUS,15.0000Panasonic,13.6667Philips,11.0000Samsumg,14.0000Toshiba,14.0000Xerox,14.0000', 'select maker, avg(screen_d) from product, laptop where product.model=laptop.model group by maker\r\n', '2', '2', '1', '1.68733589947E+39');
INSERT INTO `zapros` VALUES ('15', 'Найдите производителей, выпускающих по меньшей мере две различных модели ПК. Вывести: Maker, число моделей\r\n', '2', '5', 'Acer,2Apple,2ASUS,3Panasonic,3Toshiba,3', 'select maker, count(*)  from pc, product where pc.model=product.model group by maker having count(*)>=2\r\n', '2', '2', '1', '1.97861377137E+39');
INSERT INTO `zapros` VALUES ('16', 'Найдите максимальную цену ПК, выпускаемых каждым производителем. Вывести: maker, максимальная цена.\r\n', '2', '7', 'Acer,24000Apple,23500ASUS,15600Panasonic,13600Philips,29000Samsumg,10600Toshiba,19000', 'select maker, max(price)  from pc p,  product p2 where p.model=p2.model group by maker \r\n', '2', '2', '1', '2.29263832164E+39');
INSERT INTO `zapros` VALUES ('17', 'Для каждого значения скорости ПК, превышающего 200 МГц, определите среднюю цену ПК с такой же скоростью. Вывести: speed_mg, средняя цена.\r\n', '2', '4', '256,8093.3333266,15066.6667300,9760.0000333,13600.0000', 'select speed_mg, avg(price) from pc where speed_mg>200 group by speed_mg', '2', '2', '1', '1.85903981961E+39');
INSERT INTO `zapros` VALUES ('19', 'Найдите производителей принтеров, которые производят ПК с наименьшим объемом ram_mb и с самым быстрым процессором среди всех ПК, имеющих наименьший объем ram_mb. Вывести: Maker\r\n', '1', '1', 'Panasonic', 'select distinct maker from product where  maker in (select maker from pc, product where pc.model=product.model and ram_mb= (select min(ram_mb) from pc where model in(select model from pc where speed_mg = (select max(speed_mg) from pc))))\r\n', '3', '3', '1', '1.03279558533E+38');
INSERT INTO `zapros` VALUES ('20', 'Найдите среднюю цену ПК и принтеров. Вывести: одна общая средняя цена.\r\n', '1', '1', '9287.6666666665', 'select((SELECT avg(price)  FROM  pc, product  where pc.model=product.model )+(SELECT avg(price)  FROM  printer, product  where printer.model=product.model))/2\r\n', '3', '3', '1', '2.8822669583E+38');
INSERT INTO `zapros` VALUES ('21', 'Найдите средний размер диска ПК каждого из тех производителей, которые выпускают и принтеры. Вывести: maker, средний размер hd_gb.\r\n', '2', '4', 'Acer,120Panasonic,165.33333333333334Philips,160Toshiba,152', 'SELECT  maker,avg(hd_gb)  FROM  pc, product   where pc.model=product.model and maker in (select maker from product where type=\'printer\') group by maker\r\n', '3', '3', '1', '1.1577973648631E+39');
INSERT INTO `zapros` VALUES ('22', 'Найдите средний размер диска ПК (одно значение для всех) тех производителей, которые выпускают и принтеры. Вывести: средний размер hd_gb', '1', '1', '150.22222222222223', 'SELECT  avg(hd_gb)  FROM  pc, product   where pc.model=product.model and maker in (select maker from product where type=\'printer\')\r\n', '3', '3', '1', '1.8186955782087E+38');
INSERT INTO `zapros` VALUES ('25', '10+5', '1', '1', '15', 'select 10+5', '3', '3', '1', '2.07292645527E+38');
INSERT INTO `zapros` VALUES ('26', '6+6', '1', '1', '12', 'select 6+6', '0', '0', '2', '2.5792647109E+38');
